

# Article Collection


### Get Started ###

* [Android Studio Preview](https://developer.android.com/studio/preview/)
* [Android JetPack](https://developer.android.com/jetpack/)
* [Java 1.8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)



## Project


This project leverages on several modern approaches to build Android applications :

- 100% written in Kotlin programming language.
- [MVVM Architecture](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93viewmodel). powered by Android JetPack
- Easy code modularization 
    - All packages is organized following the pattern "package by feature", that way is easily to modularize the project when it expand and also in case the teams who works based in features is a good strategy to modularize it by team.
- Based on [Material Design](https://material.io/design/) guideline.
- Single Activity application
- Clean Code practices


## Libraries
- Koin
- RxJava/RxAndroid
- Architecture JetPack
    - Paging Library
	- Live Data
	- ViewModel
    - Room 
    - Navigation
- Retroit
- Glide
- Lottie

## Screenshots 

You may check some [screenshoots](https://bitbucket.org/marcosgribel_team/article_collection/src/8101a169dbeb43b248b032135760fb87781a8784/screenshots/?at=master) from the result application in screenshot gallery. 


## Conclusion

This project is focused to apply the best practices regarding architecture and code development, following the MVVM and Clean Archictecure and powered for Android JetPack components. 
Unfortunately Robolectrit isn't supporing the package "andoidx" yet and I couldn't developed the tests, I really like to apply tests during the development lifecycle and in case you would like to check feel free to walkthrough my repository.



# Thank you! :)

