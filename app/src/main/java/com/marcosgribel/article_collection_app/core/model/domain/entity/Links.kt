package com.marcosgribel.article_collection.core.model.domain.entity

import com.squareup.moshi.Json

data class Links(

	@Json(name="webShopUrl")
	val webShopUrl: WebShopUrl? = null,

	@Json(name="self")
	val self: Self? = null
)