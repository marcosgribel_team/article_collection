package com.marcosgribel.article_collection_app.core.rx

import io.reactivex.Scheduler

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface SchedulerProvider {

    fun mainThread(): Scheduler

    fun trampoline(): Scheduler

    fun io(): Scheduler
    
    fun computation(): Scheduler

    fun newThread(): Scheduler

}