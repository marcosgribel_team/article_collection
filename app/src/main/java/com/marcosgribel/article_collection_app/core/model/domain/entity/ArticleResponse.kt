package com.marcosgribel.article_collection.core.model.domain.entity

import com.squareup.moshi.Json

data class ArticleResponse(

	@field:Json(name="_links")
	val links: Links? = null,

	@field:Json(name="_embedded")
	val embedded: Embedded,

	@field:Json(name="articlesCount")
	val articlesCount: Int? = null,

	@field:Json(name="resourceType")
	val resourceType: String? = null
) {
	constructor() : this(null, Embedded(arrayListOf()), null, null)
}