package com.marcosgribel.article_collection.core.model.domain.entity

import com.squareup.moshi.Json

data class Embedded(

	@field:Json(name="articles")
	val articles: List<Article>
)