package com.marcosgribel.article_collection_app.core.helper

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class PreferenceHelper constructor(private val context: Context, private val prefFileName: String) {


    companion object {
        const val NUM_ARTICLE = "NUM_ARTICLE"
        const val PARAM_MIN_COUNT_ARTICLE = 5
    }


    private val preference: SharedPreferences
        get() {
            return context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)
        }


    var numArticles: Int
        get() = preference.getInt(NUM_ARTICLE, PARAM_MIN_COUNT_ARTICLE).let {
            if(it == 0) PARAM_MIN_COUNT_ARTICLE
            else it

        }
        set(value) {
            preference.edit().putInt(NUM_ARTICLE, value).apply()
        }

}