package com.marcosgribel.article_collection.core.model.domain.entity

import com.squareup.moshi.Json

data class WebShopUrl(

	@Json(name="href")
	val href: Any? = null
)