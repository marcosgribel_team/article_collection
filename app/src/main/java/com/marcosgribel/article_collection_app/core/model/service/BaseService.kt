package com.marcosgribel.article_collection_app.core.model.service

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class BaseService {

    internal fun <T> get(network: NetworkModule, clazz: Class<T>): T {
        return network.provideRetrofit().create(clazz)
    }


}