package com.marcosgribel.article_collection_app.core.model.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.marcosgribel.article_collection.core.model.domain.entity.Media
import io.reactivex.Flowable

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Dao
interface MediaDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(medias: List<Media>)

    @Query("SELECT * FROM media WHERE media.articleId =:articleId")
    fun getByArticle(articleId: Int): Flowable<List<Media>>

}