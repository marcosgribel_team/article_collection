package com.marcosgribel.article_collection_app.core.extensions

import android.content.Context
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */



fun ImageView.loadUrl(url: String?, placeHolder: Int) {

    var options = RequestOptions()
    options.placeholder(placeHolder)
    Glide.with(context)
            .load(url)
            .apply(options)
            .into(this)

}


fun Context.toastShort(message: CharSequence) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
