package com.marcosgribel.article_collection_app.core.model.db


import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection.core.model.domain.entity.Media
import com.marcosgribel.article_collection_app.core.model.db.dao.ArticleDao
import com.marcosgribel.article_collection_app.core.model.db.dao.MediaDao

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */

@Database(
        entities = [Article::class, Media::class],
        exportSchema = false,
        version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun articleDao(): ArticleDao

    abstract fun mediaDao(): MediaDao

    companion object {

        fun create(context: Context, databaseName: String, useInMemory: Boolean = false): AppDatabase {
            val databaseBuilder = if (useInMemory) {
                Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java)
                        .allowMainThreadQueries()
            } else {
                Room.databaseBuilder(context, AppDatabase::class.java, databaseName)
                        .fallbackToDestructiveMigration()

            }
            return databaseBuilder
                    .build()
        }

    }


}