package com.marcosgribel.article_collection_app.core.model.db.dao

import androidx.paging.DataSource
import androidx.room.*
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection_app.core.model.domain.entity.ArticleAndMedia
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@Dao
interface ArticleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(articles: List<Article>)

    @Query("SELECT * FROM article")
    fun getAll(): Single<List<Article>>

    @Transaction
    @Query("SELECT * FROM article")
    fun getAllWithMedia(): Flowable<List<ArticleAndMedia>>

    @Transaction
    @Query("SELECT * FROM article WHERE article.liked IS NOT NULL")
    fun getAllWithMediaDataSource(): DataSource.Factory<Int, ArticleAndMedia>

    @Query("UPDATE article SET liked = 1 WHERE sku = :id")
    fun like(id: String)

    @Query("UPDATE article SET liked = 0 WHERE sku = :id")
    fun dislike(id: String)


}