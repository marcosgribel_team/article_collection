package com.marcosgribel.article_collection_app.core.model.domain.enumerator

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
data class Status(val state: Int = LOADING, val message: String? = null) {

    companion object {
        const val LOADING = 100
        const val SUCCESS = 200
        const val ERROR = 300
        const val EMPTY = 400
    }

}