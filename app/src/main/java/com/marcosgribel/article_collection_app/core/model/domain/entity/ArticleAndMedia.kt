package com.marcosgribel.article_collection_app.core.model.domain.entity

import androidx.room.Embedded
import androidx.room.Relation
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection.core.model.domain.entity.Media

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
data class ArticleAndMedia (

        @Embedded
        var article: Article,

        @Relation( parentColumn = "sku", entityColumn = "articleId")
        var medias: List<Media>
){
        constructor() : this(Article(), arrayListOf())
}

