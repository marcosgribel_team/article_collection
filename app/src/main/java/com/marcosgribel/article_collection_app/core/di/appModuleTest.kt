package com.marcosgribel.article_collection_app.core.di

import com.marcosgribel.article_collection_app.R
import com.marcosgribel.article_collection_app.article.model.repository.ArticleRepository
import com.marcosgribel.article_collection_app.article.model.repository.ArticleRepositoryImpl
import com.marcosgribel.article_collection_app.article.model.service.ArticleService
import com.marcosgribel.article_collection_app.article.model.service.ArticleServiceImpl
import com.marcosgribel.article_collection_app.article.model.service.api.ArticleApi
import com.marcosgribel.article_collection_app.article.review.view.viewmodel.ArticleReviewViewModel
import com.marcosgribel.article_collection_app.article.selection.view.viewmodel.ArticleSelectionViewModel
import com.marcosgribel.article_collection_app.article.settings.view.viewmodel.ArticleSettingsViewModel
import com.marcosgribel.article_collection_app.core.helper.PreferenceHelper
import com.marcosgribel.article_collection_app.core.model.db.AppDatabase
import com.marcosgribel.article_collection_app.core.model.service.BaseService
import com.marcosgribel.article_collection_app.core.model.service.NetworkModule
import com.marcosgribel.article_collection_app.core.rx.AppScheduler
import com.marcosgribel.article_collection_app.core.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */


private val CoreModule = module {

    single { NetworkModule() }
    single { BaseService() }

    single { PreferenceHelper(androidContext(), androidContext().getString(R.string.shared_preference_file_name)) }
    single { AppDatabase.create(androidContext(), androidContext().getString(R.string.database_name)) }

    single { get<AppDatabase>().articleDao() }
    single { get<AppDatabase>().mediaDao() }


}

private val RxModule = module {

    single<SchedulerProvider> { AppScheduler() }

}

private val ArticleModule = module {

    single { get<BaseService>().get(get(), ArticleApi::class.java) }

    single<ArticleService> { ArticleServiceImpl(get()) }

    single<ArticleRepository> { ArticleRepositoryImpl(get(), get(), get(), get()) }


    val compositeDisposable = CompositeDisposable()

    viewModel { ArticleSelectionViewModel(compositeDisposable, get(), get()) }

    viewModel { ArticleSettingsViewModel(compositeDisposable, get(), get()) }

    viewModel { ArticleReviewViewModel(get()) }
}


val AppModule = listOf(CoreModule, RxModule, ArticleModule)