package com.marcosgribel.article_collection.core.model.domain.entity

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.squareup.moshi.Json


@Entity
data class Article(

        @field:Json(name = "description")
        var description: String?,

        @Ignore
        @field:Json(name = "media")
        var media: List<Media> = arrayListOf(),

        @field:Json(name = "title")
        var title: String?,

        @field:Json(name = "url")
        var url: String?,

        @PrimaryKey
        @field:Json(name = "sku")
        var sku: String,

        var liked: Boolean?

) {
        constructor() : this(null, arrayListOf(), null, null, "", null)
}
