package com.marcosgribel.article_collection_app.core.model.service

import com.marcosgribel.article_collection_app.BuildConfig
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class NetworkModule {

    internal fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(provideBaseApiUrl())
                .addConverterFactory(provideMoshiConverterFactory())
                .addCallAdapterFactory(provideRxJava2CallAdapterFactory())
                .client(provideOkHttpClient())
                .build()
    }


    private fun provideBaseApiUrl(): String = BuildConfig.BASE_API_URL


    private fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        var httpLoggingInterceptorLevel = HttpLoggingInterceptor.Level.NONE
        if (BuildConfig.DEBUG)
            httpLoggingInterceptorLevel = HttpLoggingInterceptor.Level.BASIC

        return HttpLoggingInterceptor().apply {
            level = httpLoggingInterceptorLevel
        }
    }


    private fun provideOkHttpClient(): OkHttpClient {
        return OkHttpClient()
                .newBuilder()
                .addInterceptor(provideHttpLoggingInterceptor())
                .build()

    }


    private fun provideMoshiConverterFactory(): MoshiConverterFactory {
        val moshi = Moshi.Builder()
        moshi.add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
        return MoshiConverterFactory.create(moshi.build())
    }

    private fun provideRxJava2CallAdapterFactory(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()


}