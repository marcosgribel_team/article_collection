package com.marcosgribel.article_collection.core.model.domain.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity
(foreignKeys = [ForeignKey(
        entity = Article::class,
        parentColumns = arrayOf("sku"),
        childColumns = arrayOf("articleId"),
        onDelete = ForeignKey.CASCADE
)])
data class Media(

        var articleId: String,

        @PrimaryKey(autoGenerate = true)
        var id: Int,

        @field:Json(name = "mimeType")
        var mimeType: String?,

        @field:Json(name = "priority")
        var priority: Int?,

        @field:Json(name = "uri")
        var uri: String?
) {
        constructor() : this("", 0, null, null, null)
}