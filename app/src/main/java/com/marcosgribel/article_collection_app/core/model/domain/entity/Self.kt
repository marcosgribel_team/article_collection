package com.marcosgribel.article_collection.core.model.domain.entity

import com.squareup.moshi.Json

data class Self(

	@Json(name="href")
	val href: String? = null
)