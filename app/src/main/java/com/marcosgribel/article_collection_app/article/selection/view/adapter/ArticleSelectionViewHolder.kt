package com.marcosgribel.article_collection_app.article.selection.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection_app.R
import com.marcosgribel.article_collection_app.core.extensions.loadUrl
import kotlinx.android.synthetic.main.item_article_selection.view.*

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleSelectionViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    interface ArticleSelectionCallback {

        fun onLike(position: Int)

        fun onDislike(position: Int)

        fun onSettings()

    }

    companion object {

        fun inflate(parent: ViewGroup): ArticleSelectionViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_article_selection, parent, false)
            return ArticleSelectionViewHolder(view)
        }

    }


    fun binData(article: Article, position: Int, likes: Int, dislikes: Int, total: Int, callback: ArticleSelectionCallback) {
        var media = article.media.first()
        view.image_article_item.loadUrl(media.uri, R.drawable.ic_launcher_background)

        view.fab_dislike_article.setOnClickListener(onDislikeListener(position, callback))
        view.fab_like_article.setOnClickListener(onLikeListener(position, callback))

        view.chip_count_articles.text = "$dislikes - $total - $likes"

        renderLikeOrDislike(article.liked)

    }


    private fun renderLikeOrDislike(liked: Boolean?){
        if(liked == null){
            onLikedOrDislikedNull()
        } else if(liked){
            onLiked()
        } else if(!liked){
            onDisliked()
        }

    }

    private fun onLikedOrDislikedNull(){
        view.fab_like_article.isEnabled = true
        view.fab_dislike_article.isEnabled = true
        view.fab_like_article.setImageResource(R.drawable.ic_heart_white)
        view.fab_dislike_article.setImageResource(R.drawable.ic_heart_off_white)
    }

    private fun onLiked() {
        view.fab_like_article.isEnabled = false
        view.fab_dislike_article.isEnabled = true
        view.fab_like_article.setImageResource(R.drawable.ic_heart_blue)
        view.fab_dislike_article.setImageResource(R.drawable.ic_heart_off_white)
    }

    private fun onDisliked() {
        view.fab_like_article.isEnabled = true
        view.fab_dislike_article.isEnabled = false
        view.fab_like_article.setImageResource(R.drawable.ic_heart_white)
        view.fab_dislike_article.setImageResource(R.drawable.ic_heart_off_blue)
    }

    private fun onLikeListener(position: Int, callback: ArticleSelectionCallback): View.OnClickListener {
        return View.OnClickListener { callback.onLike(position) }
    }

    private fun onDislikeListener(position: Int, callback: ArticleSelectionCallback): View.OnClickListener {
        return View.OnClickListener { callback.onDislike(position) }
    }
}