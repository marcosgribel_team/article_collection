package com.marcosgribel.article_collection_app.article.settings.view.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.marcosgribel.article_collection_app.article.model.repository.ArticleRepository
import com.marcosgribel.article_collection_app.core.helper.PreferenceHelper.Companion.PARAM_MIN_COUNT_ARTICLE
import com.marcosgribel.article_collection_app.core.rx.SchedulerProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleSettingsViewModel(
        private val compositeDisposable: CompositeDisposable,
        private val scheduler: SchedulerProvider,
        private val repository: ArticleRepository) : ViewModel() {


    val limitArticle = MutableLiveData<Int>()


    private var limit
        get() = repository.getNumMaxOfArticles()
        set(value) {
            repository.setNumMaxOfArticles(value)
        }


    private val btnPlusSubject = PublishSubject.create<Int>()
    private val btnMinusSubject = PublishSubject.create<Int>()


    init {

        addBtnActionSubscriber(btnPlusSubject)
        addBtnActionSubscriber(btnMinusSubject)

        limitArticle.postValue(limit)
    }


    fun onPlusClick() {
        val count = limit + PARAM_MIN_COUNT_ARTICLE
        btnPlusSubject.onNext(count)
    }

    fun onMinusClick() {
        if (limit == PARAM_MIN_COUNT_ARTICLE) return

        val count = limit - PARAM_MIN_COUNT_ARTICLE
        btnMinusSubject.onNext(count)
    }


    private fun addBtnActionSubscriber(subject: PublishSubject<Int>) {
        val disposable = subject
                .map { count ->
                    limit = count
                    count
                }.debounce(100, TimeUnit.MILLISECONDS)
                .distinctUntilChanged()
                .subscribeOn(scheduler.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    limitArticle.postValue(it)
                }, {
                    Timber.e(it)
                })


        compositeDisposable.add(disposable)
    }


    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }


}