package com.marcosgribel.article_collection_app.article.review.view.ui


import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.article_collection_app.R
import com.marcosgribel.article_collection_app.article.review.view.adapter.ArticleReviewAdapter
import com.marcosgribel.article_collection_app.article.review.view.viewmodel.ArticleReviewViewModel
import com.marcosgribel.article_collection_app.core.model.domain.entity.ArticleAndMedia
import kotlinx.android.synthetic.main.fragment_article_review.*
import org.koin.android.ext.android.inject


/**
 * A simple [Fragment] subclass.
 *
 */
class ArticleReviewFragment : Fragment() {


    private val viewModel: ArticleReviewViewModel by inject()

    private var adapter = ArticleReviewAdapter(ArticleReviewAdapter.TYPE_LIST)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_review, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }


    /**

    MENU

     */

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        activity?.menuInflater?.inflate(R.menu.menu_article_review_fragment, menu)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_list_action -> {
                onLoadListLayout()
                true
            }
            R.id.menu_grid_action -> {
                onGridLayout()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }

    }


    private fun onLoadListLayout() {
        val linearLayoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        recycler_view_of_articles_review.layoutManager = linearLayoutManager
        recycler_view_of_articles_review.setHasFixedSize(true)
        recycler_view_of_articles_review.adapter = adapter

        adapter.viewType = ArticleReviewAdapter.TYPE_LIST

    }

    private fun onGridLayout() {
        val linearLayoutManager = GridLayoutManager(context, 2)
        recycler_view_of_articles_review.layoutManager = linearLayoutManager
        recycler_view_of_articles_review.setHasFixedSize(true)
        recycler_view_of_articles_review.adapter = adapter

        adapter.viewType = ArticleReviewAdapter.TYPE_GRID
    }


    /**



     */


    private fun initialize() {
        onLoadListLayout()
        viewModel.listOfArticles.observe(this, onListOfArticleObserver())
    }


    private fun onListOfArticleObserver(): Observer<PagedList<ArticleAndMedia>> {
        return Observer { t ->
            adapter.submitList(t)
        }
    }
}
