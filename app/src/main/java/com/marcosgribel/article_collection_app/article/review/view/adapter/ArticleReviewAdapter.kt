package com.marcosgribel.article_collection_app.article.review.view.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.article_collection_app.core.model.domain.entity.ArticleAndMedia

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleReviewAdapter(var viewType: Int) : PagedListAdapter<ArticleAndMedia, RecyclerView.ViewHolder>(COMPARATOR_ITEM_CALLBACK) {


    companion object {

        private val COMPARATOR_ITEM_CALLBACK = object : DiffUtil.ItemCallback<ArticleAndMedia>() {

            override fun areItemsTheSame(oldItem: ArticleAndMedia, newItem: ArticleAndMedia): Boolean = oldItem.article.sku == newItem.article.sku


            override fun areContentsTheSame(oldItem: ArticleAndMedia, newItem: ArticleAndMedia): Boolean = oldItem.article.title == newItem.article.title
        }


        const val TYPE_GRID = 1
        const val TYPE_LIST = 2

    }

    override fun getItemViewType(position: Int): Int = viewType


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_GRID -> ArticleReviewGridViewHolder.inflate(parent)
            TYPE_LIST -> ArticleReviewListViewHolder.inflate(parent)
            else -> throw RuntimeException("ViewHolder not found.")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val articleAndMedia = getItem(position)
        if (holder is ArticleReviewGridViewHolder) {
            holder.bindData(articleAndMedia!!)
        } else if (holder is ArticleReviewListViewHolder) {
            holder.bindData(articleAndMedia!!)
        }

    }
}