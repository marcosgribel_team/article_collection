package com.marcosgribel.article_collection_app.article.model.service

import com.marcosgribel.article_collection.core.model.domain.entity.ArticleResponse
import com.marcosgribel.article_collection_app.article.model.service.api.ArticleApi
import io.reactivex.Single

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface ArticleService {

    fun search(limit: Int): Single<ArticleResponse>

}

class ArticleServiceImpl (private val api: ArticleApi) : ArticleService {


    override fun search(limit: Int): Single<ArticleResponse> = api.get(limit)

}