package com.marcosgribel.article_collection_app.article.model.service.api

import com.marcosgribel.article_collection.core.model.domain.entity.ArticleResponse
import com.marcosgribel.article_collection_app.core.helper.PreferenceHelper.Companion.PARAM_MIN_COUNT_ARTICLE
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface ArticleApi {

    @GET("api/v1/articles?appDomain=1")
    fun get(
            @Query("limit") limit: Int = PARAM_MIN_COUNT_ARTICLE,
            @Query("language") locale: String? = "de_DE"
    ) : Single<ArticleResponse>

}