package com.marcosgribel.article_collection_app.article.review.view.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.marcosgribel.article_collection_app.article.model.repository.ArticleRepository
import com.marcosgribel.article_collection_app.core.model.domain.entity.ArticleAndMedia

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleReviewViewModel(
        repository: ArticleRepository
) : ViewModel() {


    var listOfArticles: LiveData<PagedList<ArticleAndMedia>> = MutableLiveData<PagedList<ArticleAndMedia>>()


    init {

        val numMaxOfArticles = repository.getNumMaxOfArticles()

        val pagedListConfig = PagedList.Config.Builder()
                .setInitialLoadSizeHint(numMaxOfArticles)
                .setPageSize(numMaxOfArticles)
                .setPrefetchDistance(numMaxOfArticles)
                .setEnablePlaceholders(false)
                .build()

        val dataSource = repository.getFromDb()
        listOfArticles = LivePagedListBuilder<Int, ArticleAndMedia>(dataSource, pagedListConfig).build()

    }
}