package com.marcosgribel.article_collection_app.article.selection.view.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.marcosgribel.article_collection.article.selection.datasource.ArticleSelectionDataSourceFactory
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection_app.article.model.repository.ArticleRepository
import com.marcosgribel.article_collection_app.core.model.domain.enumerator.Status
import com.marcosgribel.article_collection_app.core.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleSelectionViewModel constructor(
        private val compositeDisposable: CompositeDisposable,
        private val scheduler: SchedulerProvider,
        private val repository: ArticleRepository


) : ViewModel() {


//    private val numMaxOfArticles: Int = repository.getNumMaxOfArticles()


    var status = MutableLiveData<Status>()
    var listOfArticles: LiveData<PagedList<Article>> = MutableLiveData<PagedList<Article>>()



    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }

    fun onLoad(){

        val numMaxOfArticles = repository.getNumMaxOfArticles()
        val dataSourceFactory = ArticleSelectionDataSourceFactory(compositeDisposable, scheduler, repository, status)
        val pagedListConfig = PagedList.Config.Builder()
                .setInitialLoadSizeHint(numMaxOfArticles)
                .setPageSize(numMaxOfArticles)
                .setPrefetchDistance(1)
                .setEnablePlaceholders(false)
                .build()

        listOfArticles = LivePagedListBuilder<String, Article>(dataSourceFactory, pagedListConfig).build()

    }


    fun like(sku: String) {
        val disposable = repository.like(sku)
                .subscribeOn(scheduler.io()).observeOn(scheduler.mainThread())
                .subscribe({

                }, {
                    status.postValue(Status(Status.ERROR, it.message))
                })

        compositeDisposable.add(disposable)
    }


    fun dislike(sku: String) {
        val disposable = repository.dislike(sku)
                .subscribeOn(scheduler.io()).observeOn(scheduler.mainThread())
                .subscribe({

                }, {
                    status.postValue(Status(Status.ERROR, it.message))
                })

        compositeDisposable.add(disposable)

    }

    fun canEnableReview(list: List<Article>?): Boolean {

        return when {
            list == null -> false
            list.isEmpty() -> false
            else -> {
                val count = list.count {
                    it.liked == null
                }
                count == 0
            }
        }
    }
}
