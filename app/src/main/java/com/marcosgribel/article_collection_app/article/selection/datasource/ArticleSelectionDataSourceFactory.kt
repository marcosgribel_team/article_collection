package com.marcosgribel.article_collection.article.selection.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection_app.article.model.repository.ArticleRepository
import com.marcosgribel.article_collection_app.core.model.domain.enumerator.Status
import com.marcosgribel.article_collection_app.core.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleSelectionDataSourceFactory constructor(private val compositeDisposable: CompositeDisposable,
                                                    private val scheduler: SchedulerProvider,
                                                    private val repository: ArticleRepository,
                                                    private val status: MutableLiveData<Status>) : DataSource.Factory<String, Article>() {

   private val dataSource = MutableLiveData<ArticleSelectionDataSource>()

    override fun create(): DataSource<String, Article> {
        val articleDataSource = ArticleSelectionDataSource(compositeDisposable, scheduler, repository, status)
        dataSource.postValue(articleDataSource)
        return articleDataSource
    }
}