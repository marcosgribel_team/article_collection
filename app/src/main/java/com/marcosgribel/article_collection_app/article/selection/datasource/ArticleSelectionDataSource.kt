package com.marcosgribel.article_collection.article.selection.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.ItemKeyedDataSource
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection_app.article.model.repository.ArticleRepository
import com.marcosgribel.article_collection_app.core.model.domain.enumerator.Status
import com.marcosgribel.article_collection_app.core.rx.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleSelectionDataSource constructor(
        private val compositeDisposable: CompositeDisposable,
        private val scheduler: SchedulerProvider,
        private val repository: ArticleRepository,
        private val status: MutableLiveData<Status>
) : ItemKeyedDataSource<String, Article>() {


    init {
        val tag = ArticleSelectionDataSource::class.java.simpleName.toString()
        Timber.tag(tag)

    }

    interface ArticleSelectionDataSourceCallback {

        fun onSuccess(list: List<Article>)

        fun onFailure(throwable: Throwable)

    }


    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<Article>) {

    }

    override fun loadInitial(params: LoadInitialParams<String>, callback: LoadInitialCallback<Article>) {
        onLoad(object : ArticleSelectionDataSourceCallback {
            override fun onSuccess(list: List<Article>) {
                callback.onResult(list)
            }

            override fun onFailure(throwable: Throwable) {
                loadInitial(params, callback)
            }
        })
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<Article>) {

        onLoad(object : ArticleSelectionDataSourceCallback {
            override fun onSuccess(list: List<Article>) {
                callback.onResult(list)
            }

            override fun onFailure(throwable: Throwable) {
                loadAfter(params, callback)
            }
        })

    }

    override fun getKey(item: Article): String = item.sku



    private fun onLoad(callback: ArticleSelectionDataSourceCallback) {

        status.postValue(Status(Status.LOADING))

        val limit = repository.getNumMaxOfArticles()

        val disposable = repository.getFromApi(limit)
                .subscribeOn(scheduler.io())
                .observeOn(scheduler.mainThread())
                .subscribe({
                    status.postValue(Status(Status.SUCCESS))

                    callback.onSuccess(it)
                }, {
                    Timber.e(it)
                    status.postValue(Status(Status.ERROR))
                    callback.onFailure(it)
                })

        compositeDisposable.add(disposable)
    }
}