package com.marcosgribel.article_collection_app.article.model.repository

import androidx.paging.DataSource
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection_app.article.model.service.ArticleService
import com.marcosgribel.article_collection_app.core.helper.PreferenceHelper
import com.marcosgribel.article_collection_app.core.model.db.dao.ArticleDao
import com.marcosgribel.article_collection_app.core.model.db.dao.MediaDao
import com.marcosgribel.article_collection_app.core.model.domain.entity.ArticleAndMedia
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
interface ArticleRepository {

    fun getFromApi(limit: Int): Single<List<Article>>

    fun getFromDb(): DataSource.Factory<Int, ArticleAndMedia>

    fun like(sku: String): Completable

    fun dislike(sku: String): Completable

    fun getNumMaxOfArticles(): Int

    fun setNumMaxOfArticles(value: Int)
}


class ArticleRepositoryImpl(
        private val service: ArticleService,
        private val articleDao: ArticleDao,
        private val mediaDao: MediaDao,
        private val preference: PreferenceHelper
) : ArticleRepository {


    override fun getFromApi(limit: Int): Single<List<Article>> {

        return service.search(limit)
                .flatMap {
                    val articles = it.embedded.articles

                    // Merge the field 'liked' from API to DB
                    return@flatMap articleDao.getAll().map {
                        articles.forEach { a1 ->
                            a1.liked = it.firstOrNull { a2 -> a1.sku == a2.sku }?.liked
                        }

                        save(articles)

                        return@map articles
                    }
                }
    }


    private fun save(articles: List<Article>) {
        articleDao.save(articles)
        articles.forEach { article ->
            article.media.forEach { media ->
                media.articleId = article.sku
            }
            mediaDao.save(article.media)
        }


    }


    override fun like(sku: String): Completable {
        return Completable.create {
            articleDao.like(sku)
            it.onComplete()
        }
    }


    override fun dislike(sku: String): Completable {
        return Completable.create {
            articleDao.dislike(sku)
            it.onComplete()
        }
    }

    override fun getNumMaxOfArticles() : Int  = preference.numArticles



    override fun setNumMaxOfArticles(value: Int) {
        preference.numArticles = value
    }


    override fun getFromDb(): DataSource.Factory<Int, ArticleAndMedia> = articleDao.getAllWithMediaDataSource()
}