package com.marcosgribel.article_collection_app.article.selection.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.article_collection_app.R
import kotlinx.android.synthetic.main.item_empyt_article_selection.view.*

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleSelectionEmptyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

    companion object {

        fun inflate(parent: ViewGroup): ArticleSelectionEmptyViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_empyt_article_selection, parent, false)
            return ArticleSelectionEmptyViewHolder(view)
        }

    }


    fun bindData(callback: ArticleSelectionViewHolder.ArticleSelectionCallback){
        view.button_settings.setOnClickListener(onSettingsClickListener(callback))
    }


    private fun onSettingsClickListener(callback: ArticleSelectionViewHolder.ArticleSelectionCallback) : View.OnClickListener{
        return View.OnClickListener { callback.onSettings() }
    }
}