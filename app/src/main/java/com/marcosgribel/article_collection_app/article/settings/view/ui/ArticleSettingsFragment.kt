package com.marcosgribel.article_collection_app.article.settings.view.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.marcosgribel.article_collection_app.R
import com.marcosgribel.article_collection_app.article.settings.view.viewmodel.ArticleSettingsViewModel
import kotlinx.android.synthetic.main.fragment_article_settings.*
import org.koin.android.ext.android.inject


/**
 * A simple [Fragment] subclass.
 *
 */
class ArticleSettingsFragment : Fragment() {


    private val viewModel: ArticleSettingsViewModel by inject()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_settings, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initialize()

    }


    private fun initialize() {

        image_button_plus.setOnClickListener(onBtnPlusClick())
        image_button_minus.setOnClickListener(onBtnMinusClick())

        viewModel.limitArticle.observe(this, onLimitArticleObserver())
    }


    private fun onBtnPlusClick(): View.OnClickListener {
        return View.OnClickListener { viewModel.onPlusClick() }
    }

    private fun onBtnMinusClick(): View.OnClickListener {
        return View.OnClickListener { viewModel.onMinusClick() }
    }


    private fun onLimitArticleObserver(): Observer<Int> {
        return Observer { count -> text_count_limit_articles.text = count.toString() }
    }



}
