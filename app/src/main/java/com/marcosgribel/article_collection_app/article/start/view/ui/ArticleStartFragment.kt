package com.marcosgribel.article_collection_app.article.start.view.ui


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.marcosgribel.article_collection_app.MainActivity
import com.marcosgribel.article_collection_app.R
import kotlinx.android.synthetic.main.fragment_article_start.*


/**
 * A simple [Fragment] subclass.
 *
 */
class ArticleStartFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_start, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        button_start.setOnClickListener(onFabStartClickListener())
    }

    override fun onResume() {
        super.onResume()
        (activity as MainActivity).hideActionBar()
    }

    override fun onPause() {
        (activity as MainActivity).showActionBar()
        super.onPause()
    }




    private fun onFabStartClickListener(): View.OnClickListener {
        return View.OnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_articleStartFragment_to_articleSelectionFragment)
        }
    }
}
