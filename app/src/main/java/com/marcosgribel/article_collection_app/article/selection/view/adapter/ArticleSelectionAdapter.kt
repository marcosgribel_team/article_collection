package com.marcosgribel.article_collection_app.article.selection.view.adapter

import android.view.ViewGroup
import androidx.lifecycle.LiveData
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection_app.core.model.domain.enumerator.Status

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleSelectionAdapter(
        private val status: LiveData<Status>,
        private val callback: ArticleSelectionViewHolder.ArticleSelectionCallback
) : PagedListAdapter<Article, RecyclerView.ViewHolder>(COMPARATOR_ITEM_CALLBACK) {

    companion object {

        private val COMPARATOR_ITEM_CALLBACK = object : DiffUtil.ItemCallback<Article>() {

            override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean = oldItem.sku == newItem.sku

            override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean = oldItem.title == newItem.title
        }


        const val TYPE_ITEM = 1
        const val TYPE_FOOTER = 2

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ITEM -> ArticleSelectionViewHolder.inflate(parent)
            TYPE_FOOTER -> ArticleSelectionEmptyViewHolder.inflate(parent)
            else -> throw RuntimeException("ViewHolder not found.")
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ArticleSelectionViewHolder) {
            val article = getItem(position)!!
            val likes = getLikeCount()
            val dislikes = getDisLikeCount()
            val total = itemCount - 1
            holder.binData(article, position, likes, dislikes, total, callback)
        } else if (holder is ArticleSelectionEmptyViewHolder) {
            holder.bindData(callback)
        }
    }

    fun getArticle(position: Int): Article {
        return super.getItem(position)!!
    }

    override fun getItemViewType(position: Int): Int {
        return if (isFooter(position)) TYPE_FOOTER
        else TYPE_ITEM

        return super.getItemViewType(position)
    }


    override fun getItemCount(): Int {
        return when {
            this.currentList == null -> return 0
            this.currentList!!.size == 0 -> return 1
            else -> this.currentList!!.size + 1
        }
    }

    private fun getLikeCount(): Int {
        return this.currentList?.count { it.liked == true } ?: 0
    }

    private fun getDisLikeCount(): Int {
        return this.currentList?.count { it.liked == false } ?: 0
    }


    private fun isFooter(position: Int): Boolean {
        return (position == (currentList?.size ?: 0) && (status.value?.state != Status.LOADING))
    }
}

