package com.marcosgribel.article_collection_app.article.selection.view.ui


import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection_app.R
import com.marcosgribel.article_collection_app.article.selection.view.adapter.ArticleSelectionAdapter
import com.marcosgribel.article_collection_app.article.selection.view.adapter.ArticleSelectionViewHolder
import com.marcosgribel.article_collection_app.article.selection.view.viewmodel.ArticleSelectionViewModel
import com.marcosgribel.article_collection_app.core.extensions.toastShort
import com.marcosgribel.article_collection_app.core.model.domain.enumerator.Status
import kotlinx.android.synthetic.main.fragment_article_selection.*
import org.koin.android.ext.android.inject

/**
 * A simple [Fragment] subclass.
 *
 */
class ArticleSelectionFragment : Fragment() {


    private val viewModel: ArticleSelectionViewModel by inject()

    private lateinit var adapter: ArticleSelectionAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_selection, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialize()
    }


    /**

    MENU

     */

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        activity?.menuInflater?.inflate(R.menu.menu_article_selection_fragment, menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        return when (item?.itemId) {
            R.id.menu_review_action -> {
                onReviewMenuItemClicked()
                true
            }
            R.id.menu_settings_action -> {
                onSettingsMenuItemClicked()
                true
            }
            else -> return super.onOptionsItemSelected(item)
        }

    }

    private fun onReviewMenuItemClicked() {
        if (viewModel.canEnableReview(adapter.currentList)) {
            Navigation.findNavController(view!!).navigate(R.id.action_articleSelectionFragment_to_articleReviewFragment)
        } else {
            context?.toastShort(getString(R.string.msg_review_menu_button_active_after_rate_all_articles))
        }
    }


    private fun onSettingsMenuItemClicked() {
        Navigation.findNavController(view!!).navigate(R.id.action_articleSelectionFragment_to_articleSettingsFragment)

    }


    /**

    Other methods

     */

    private fun initialize() {
        onLoad()

        adapter = ArticleSelectionAdapter(viewModel.status, onArticleSelectionCallback())

        val linearLayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        recycler_view_of_articles.layoutManager = linearLayoutManager
        recycler_view_of_articles.setHasFixedSize(true)
        recycler_view_of_articles.adapter = adapter


        viewModel.status.observe(this, onStatusObserver())
        viewModel.listOfArticles.observe(this, onListOfArticlesObserver())
    }

    private fun onLoad(){
        viewModel.onLoad()
    }

    private fun onListOfArticlesObserver(): Observer<PagedList<Article>> {
        return Observer { t ->
            adapter.submitList(t)
        }
    }

    private fun onStatusObserver(): Observer<Status> {
        return Observer {
            when (it.state) {
                Status.LOADING -> onLoading()
                Status.SUCCESS -> onSuccess()
                Status.ERROR -> onError(it.message)
            }
        }
    }


    private fun onArticleSelectionCallback(): ArticleSelectionViewHolder.ArticleSelectionCallback {
        return object : ArticleSelectionViewHolder.ArticleSelectionCallback {
            override fun onSettings() {
                onSettingsMenuItemClicked()
            }

            override fun onLike(position: Int) {
                val article = adapter.getArticle(position)
                viewModel.like(article.sku)
                onLikeAndDislikeClicked(position, true)
            }

            override fun onDislike(position: Int) {
                val article = adapter.getArticle(position)
                viewModel.dislike(article.sku)
                onLikeAndDislikeClicked(position, false)
            }
        }



    }


    private fun onLikeAndDislikeClicked(position: Int, liked: Boolean) {
        val article = adapter.getArticle(position)
        article.liked = liked
        adapter.notifyDataSetChanged()
        recycler_view_of_articles.scrollToPosition(position + 1)
    }


    private fun onLoading() {
        image_loading_list_of_articles.visibility = View.VISIBLE
        recycler_view_of_articles.visibility = View.GONE
    }

    private fun onSuccess() {
        image_loading_list_of_articles.visibility = View.GONE
        recycler_view_of_articles.visibility = View.VISIBLE
    }

    private fun onError(message: String?) {
        message?.let {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        }
    }



}
