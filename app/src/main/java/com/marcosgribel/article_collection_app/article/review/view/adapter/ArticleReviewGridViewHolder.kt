package com.marcosgribel.article_collection_app.article.review.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.marcosgribel.article_collection_app.R
import com.marcosgribel.article_collection_app.core.extensions.loadUrl
import com.marcosgribel.article_collection_app.core.model.domain.entity.ArticleAndMedia
import kotlinx.android.synthetic.main.item_article_review_list.view.*

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class ArticleReviewGridViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


    companion object {

        fun inflate(parent: ViewGroup): ArticleReviewGridViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_article_review_grid, parent, false)
            return ArticleReviewGridViewHolder(view)
        }

    }



    fun bindData(data: ArticleAndMedia){

        val article = data.article
        val media = data.medias.firstOrNull()

        view.image_article_image.loadUrl(media?.uri, R.drawable.ic_launcher_background)

        view.image_icon_liked_or_disliked.visibility = View.INVISIBLE

        article.liked?.let {
            view.image_icon_liked_or_disliked.visibility = View.VISIBLE
            if(it)
                view.image_icon_liked_or_disliked.setImageResource(R.drawable.ic_heart_red)
            else
                view.image_icon_liked_or_disliked.setImageResource(R.drawable.ic_heart_off_gray)
        }

    }

}