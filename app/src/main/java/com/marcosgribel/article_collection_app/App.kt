package com.marcosgribel.article_collection_app

import android.app.Application
import com.marcosgribel.article_collection_app.core.di.AppModule
import org.koin.android.ext.android.startKoin

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, AppModule)
    }
}