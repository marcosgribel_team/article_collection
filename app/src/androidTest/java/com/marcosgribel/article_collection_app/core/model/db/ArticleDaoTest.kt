package com.marcosgribel.article_collection_app.core.model.db

import androidx.test.runner.AndroidJUnit4
import com.marcosgribel.article_collection.core.model.domain.entity.Article
import com.marcosgribel.article_collection.core.model.domain.entity.ArticleResponse
import com.marcosgribel.article_collection_app.core.helper.FileReaderHelper
import com.marcosgribel.article_collection_app.core.model.db.dao.ArticleDao
import com.marcosgribel.article_collection_app.core.model.db.dao.MediaDao
import com.marcosgribel.article_collection_app.core.model.domain.entity.ArticleAndMedia
import com.squareup.moshi.Moshi
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by marcosgribel.
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 *
 */
@RunWith(AndroidJUnit4::class)
class ArticleDaoTest : AppDatabaseTest() {

    lateinit var fakeListOfArticles: List<Article>
    lateinit var articleDao: ArticleDao
    lateinit var mediaDao: MediaDao

    companion object {

        const val NUM_ARTICLE_JSON = 10
    }

    @Before
    override fun setUp() {
        super.setUp()

        articleDao = database.articleDao()
        mediaDao = database.mediaDao()

        val inputStream = this::class.java.classLoader.getResourceAsStream("articles.json")
        val jsonListOfArticles = FileReaderHelper.readStream(inputStream)

        val adapter = Moshi.Builder().build().adapter(ArticleResponse::class.java)
        val response = adapter.fromJson(jsonListOfArticles)!!

        fakeListOfArticles = response.embedded.articles
    }

    @After
    override fun setDown() {
        super.setDown()
    }

    @Test
    fun test_insert_articles() {

        articleDao.save(fakeListOfArticles)

        fakeListOfArticles.forEach { article ->
            article.media.forEach { media ->
                media.articleId = article.sku
            }
            mediaDao.save(article.media)
        }

        articleDao.getAll()
                .test()
                .assertValue {
                    it.size == NUM_ARTICLE_JSON
                }

        articleDao.getAllWithMedia()
                .test()
                .assertValue {
                    it.size == NUM_ARTICLE_JSON
                }


        val articleAndMedia : List<ArticleAndMedia> = articleDao.getAllWithMedia()
                .test().values()[0]

        articleAndMedia.forEach {
            Assert.assertNotNull(it.article)
            Assert.assertNotNull(it.medias)
        }

    }


    @Test
    fun test_like_article(){

        val article = fakeListOfArticles[2]
        articleDao.save(fakeListOfArticles)
        articleDao.like(article.sku)

        articleDao.getAll().test().assertValue {
            it.find { it.sku == article.sku }?.liked == true
        }
    }


    @Test
    fun test_dislike_article(){

        val article = fakeListOfArticles[2]
        articleDao.save(fakeListOfArticles)
        articleDao.dislike(article.sku)

        articleDao.getAll().test().assertValue {
            it.find { it.sku == article.sku }?.liked == false
        }
    }

}