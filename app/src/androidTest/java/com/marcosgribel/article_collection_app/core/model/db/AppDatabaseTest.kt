package com.marcosgribel.article_collection_app.core.model.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith

/**
 * Created by marcosgribel.
 *
 *
 *
 *
 * Copyright 2018 - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited.
 * Proprietary and confidential.
 *
 * @author Marcos Gribel
 */

@RunWith(AndroidJUnit4::class)
open class AppDatabaseTest {

    @get:Rule
    open var instantTaskExecutor = InstantTaskExecutorRule()


    protected var database: AppDatabase

    init {
        var targetContext = InstrumentationRegistry.getTargetContext()
        database = AppDatabase.create(targetContext, "", true)
    }

    @Before
    open fun setUp(){

    }



    @After
    open fun setDown(){
        database.clearAllTables()
        database.close()
    }

}